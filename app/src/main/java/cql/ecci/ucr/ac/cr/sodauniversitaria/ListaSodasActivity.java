package cql.ecci.ucr.ac.cr.sodauniversitaria;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ListaSodasActivity extends AppCompatActivity {

    private ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_sodas);

        listView = findViewById(R.id.list);

        String [] values = new String[] {
                "Soda 01",
                "Soda 02",
                "Soda 03",
                "Soda 04",
                "Soda 05",
                "Soda 06",
                "Soda 07",
                "Soda 08",
                "Soda 09",
                "Soda 10",
                "Soda 11",
                "Soda 12",
                "Soda 13",
                "Soda 14",
                "Soda 15",
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
                android.R.id.text1, values);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                int itemPosition = position;
                String itemValue = (String) listView.getItemAtPosition(position);
                Toast.makeText(getApplicationContext(), "Position: " + itemPosition + " List item: " + itemValue,
                        Toast.LENGTH_LONG).show();
            }
        });

    }
}
