package cql.ecci.ucr.ac.cr.sodauniversitaria;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "persona";
    private static final int SECOND_ACTIVITY_RESULT_CODE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Crea la actividad principal
        super.onCreate(savedInstanceState);
        //Asigna el layout
        setContentView(R.layout.activity_main);

        //Creación de las clases de la lógica del negocio
        final Persona miPersona = new Persona();
        final Soda miSoda = new Soda();

        //Se instancian los textos y botones del layout
        TextView persona = findViewById(R.id.persona);
        Button buttonPagina = findViewById(R.id.buttonPagina);
        Button buttonLlamar = findViewById(R.id.buttonLlamar);
        Button buttonCorreo = findViewById(R.id.buttonCorreo);
        Button buttonMapa = findViewById(R.id.buttonMapa);
        Button buttonCalcularPropina = findViewById(R.id.buttonCalcularPropina);
        Button buttonListaSodas = findViewById(R.id.buttonListaSodas);

        //Mensaje de bienvenida para la persona
        persona.setText(miPersona.getNombre() + ": es un placer servirle.");

        //Asocia los eventos de click a cada uno de los botones

        buttonPagina.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                irPagina(miSoda);
            }
        });

        buttonLlamar.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                realizarLlamada(miSoda);
            }
        });

        buttonCorreo.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                enviarCorreo(miSoda);
            }
        });

        buttonMapa.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                irMapa(miSoda);
            }
        });

        buttonCalcularPropina.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                irCalculadoraPropina(miPersona, miSoda);
            }
        });

        buttonListaSodas.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                irListaSodas(miSoda);
            }
        });
    }

    //Va a la página de soda
    private void irPagina(Soda soda){
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(soda.getWebsite()));
        startActivity(intent);
    }

    private void realizarLlamada(Soda soda){
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + soda.getTelefono()));
        startActivity(intent);
    }

    private void enviarCorreo(Soda soda){
        String [] TO = {soda.getEmail()};
        Uri uri = Uri.parse("mailto:" + soda.getEmail())
                .buildUpon()
                .appendQueryParameter("subject", "Consulta a la Soda Universitaria")
                .appendQueryParameter("body", "Enviado desde SodaUniversitaria.")
                .build();
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, uri);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        startActivity(Intent.createChooser(emailIntent, "Enviar vía correo"));
    }

    private void irMapa(Soda soda){
        String url = "geo:" + String.valueOf(soda.getLatitud()) + "," +
                String.valueOf(soda.getLongitud());
        String q = "?q=" + String.valueOf(soda.getLatitud()) + ", " +
                String.valueOf(soda.getLongitud()) + "(" + soda.getNombre() + ")";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url + q));
        intent.setPackage("com.google.android.apps.maps");
        startActivity(intent);
    }

    private void irCalculadoraPropina(Persona persona, Soda soda){
        Intent intent = new Intent(this, CalculadoraActivity.class);
        intent.putExtra(EXTRA_MESSAGE, persona);
        startActivityForResult(intent, SECOND_ACTIVITY_RESULT_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == SECOND_ACTIVITY_RESULT_CODE){
            if(resultCode == RESULT_OK){
                String returnString = data.getStringExtra("montoStr");
                Toast.makeText(getApplicationContext(), returnString, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void irListaSodas(Soda soda){
        Intent intent = new Intent(this, ListaSodasActivity.class);
        startActivity(intent);
    }
}
